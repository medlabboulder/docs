Example mkdocs site that publishes from GitLab to Surfer on Cloudron. Based on [https://git.cloudron.io/cloudron/docs](https://git.cloudron.io/cloudron/docs).

Currently set up to deploy to `docs.medlab.host`, but the site is no longer set up. To point to another Surfer instance, adjust the following settings accordingly:

- GitLab settings > CI/CD > Variables: `CLOUDRON_DOCS_SURFER_TOKEN`
- .gitlab-ci.yml: `DOCS_SERVER`
- files/mkdocs.yml: `site_url`

`CLOUDRON_DOCS_SURFER_TOKEN` can be found by logging in to the admin interface in Surfer.
